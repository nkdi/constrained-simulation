"""
Created on Sat Mar 14 09:49:17 2020

@author: davcon

This script incorporates measured wind time series
on Mann-generated turbulence fields. 

The theoretical appraoch is described in: 
[1] Dimitrov et al. 2017, Application of simulated lidar scanning patterns to constrained Gaussian turbulence fields for load validation, Wind Energy Journal
[2] Conti et al. 2020, Wind turbine load validation in wakes using field reconstruction techniques and nacelle lidar wind retrievals, Wind Energy Science Journal
"""


import os.path
from os import path
import pandas as pd
import numpy as np 
import matplotlib.pyplot as plt
import os 
import scipy.io
from scipy import stats
import scipy.io as sio
from MannTensor_lib import MannTensor,TrapezoidalSum2D
import cmath
from scipy.interpolate import interp1d
import scipy.linalg as linalg
import pickle 
import re


def nextpow2(x):
    """returns the smallest power of two that is greater than or equal to the
    absolute value of x.

    This function is useful for optimizing FFT operations, which are
    most efficient when sequence length is an exact power of two.

    :Example:

    .. doctest::

        >>> from spectrum import nextpow2
        >>> x = [255, 256, 257]
        >>> nextpow2(x)
        array([8, 8, 9])

    """
    res = np.ceil(np.log2(x))
    return res.astype('int') 



def ConstrainTurbulenceBox_U_S(Input):
    """
    This function ....
    ... what this function does ... 
    
    The following steps are taken:
    1) Assign input variables
    2) Compute the velocity spectral tensor from the Mann parameters 
    3) Compute the cross-correlation structure from the spectral tensor model 
    4) Compute the correlation matrix of the constraints
    5) Apply constraints
    
    """
    
    # ===========================
    # 1) Assign input variables 
    # ===========================
    
    Udatafile = Input['Udatafile']
    Constraints = Input['Constraints']
    
    if not('CalculateSpectrum' in Input.keys()):    
        CalculateSpectrum = 1
    else:
        CalculateSpectrum = Input['CalculateSpectrum']
    
    if CalculateSpectrum == 0:
        CrossSpectrumFilenames = Input['CrossSpectrumFilenames']
    
    if not('CrossSpectrumComponent1' in Input.keys()):
        CrossSpectrumComponent1 = 1
        CrossSpectrumComponent2 = 1
    else:
        CrossSpectrumComponent1 = Input['CrossSpectrumComponent1']
        CrossSpectrumComponent2 = Input['CrossSpectrumComponent2']
    
    # Mann model parameters 
    Gamma = Input['Gamma']
    L = Input['L']
    alphaepsilon = Input['AlphaEpsilon']
    kdepth = Input['SpectrumSteps']
 
    # Turbulence seed 
    tu_seed = Input['OutputFileName']
    temp=re.findall(r'\d+', tu_seed)
    tu_seed = int(temp[0])

    if not('PrintDetailedOutput' in Input.keys()):
        PrintOutput = False # use this to inspect the code
    else:
        PrintOutput = Input['PrintDetailedOutput']
    
    if not('MaxIter' in Input.keys()):
        MaxIter = 0
    
    # Turbulence box parameters 
    Nx = Input['Nx']
    Ny = Input['Ny']
    Nz = Input['Nz']
    BoxWidth = Input['BoxWidth']
    BoxHeight = Input['BoxHeight']
    
    # Simulation time
    T = Input['T']
    
    # Mean wind speed 
    Umean = Input['Umean']

    if not('SaveToFile' in Input.keys()):
        SaveToFile = 0
    else:
        SaveToFile = Input['SaveToFile']
        if ~('OutputFileName' in Input.keys()):
            OutputFileName = ('%s_c.bin' %Udatafile[0:-4])
        else:
            OutputFileName = Input['OutputFileName']
            
    
    # Resolution of the turbulence box 
    dy = BoxWidth/Ny
    dz = BoxHeight/Nz
    
    # ================================================================
    # 2) Compute the velocity spectral tensor from the Mann parameters 
    # ================================================================
    
    # ... 
    fs = Nx/T
    dt = 1/fs
    
    # ... 
    k1range = [np.max([np.log10(1e-3/L)+np.spacing(2), np.log10(np.pi/(Nx*Umean*dt))]),\
               np.min([np.log10(1e3/L)-np.spacing(2), np.log10(np.pi/(Umean*dt))])]
    # ... 
    krange = [np.min([np.log10(1e-3/L), np.log10(np.pi/(Umean*T))]),\
               np.max([np.log10(1e3/L), np.log10(np.pi/(Umean*dt))])]
    
    Nmin = 2**nextpow2(2*np.pi/(10**k1range[0]*Umean*T/Nx))
    Nfactor = Nmin/Nx
    Nx_s = Nfactor*Nx

    L0 = np.arange(1,Nx_s/2,1)
    f0 = L0/(dt*Nx_s)
    tx = dt*np.arange(0,(Nx),1)
    
    ksim = 2*np.pi*f0/Umean
    
    k1points = 64
    kmin = ksim[0]
    kmax = ksim[-1:][0]
    kvarrange = np.asarray([np.log10(kmin)-np.spacing(2), np.log10(kmax)+np.spacing(2)])
    
    k1 = 10**np.linspace(kvarrange[0],kvarrange[1],k1points)
    
    
    if CalculateSpectrum ==1:
        print('Calculating Mann spectrum.. ')
        
        if MaxIter == 0:
            Sarray = np.zeros(shape=(len(k1),Ny,Nz))
            Scomponent = 10*CrossSpectrumComponent1 + CrossSpectrumComponent2
            for ik in range(0,len(k1)):   # range(0,len(k1)):range(0,len(k1)):        
                print('ik=%s ' %ik)            
                if PrintOutput:
                    print('ik=%s ' %ik)
                
                klog0 = np.linspace(krange[0],krange[-1:][0],kdepth)
                # Compute the wavevector [k1,k2,k3] 
                k0p = (10**klog0)
                k0n = (-np.flip(10**klog0,axis=0))
                k2_p = np.concatenate((k0n,k0p), axis=0)
                k3_p = np.concatenate((k0n,k0p), axis=0)
                k2grid,k3grid=np.meshgrid(k2_p,k3_p)
                k1_in=k1[ik]*np.ones(k2grid.shape)
                k2_in=np.tile(k2_p,(len(k3_p),1))
                k3_in=np.tile(k3_p.T,(len(k2_p),1)).T
                # Compute the spectrum of the Scomponent (e.g., uu)
                PsiK = MannTensor(k1_in,k2_in,k3_in,Gamma,L,alphaepsilon,Scomponent)
                for iY in range(0,Ny):
                    for iZ in range(0,Nz):
                        dyi = (iY+1-1)*dy
                        dzi = (iZ+1-1)*dz
                        PsiIJ = PsiK*np.exp(cmath.sqrt(-1)*(k2grid*dyi + k3grid*dzi))
                        ksum = TrapezoidalSum2D(PsiIJ,k2_p,k3_p)
                        Sarray[ik,iY,iZ] = np.real(ksum)
    
            Psi0 = Sarray[:,0,0]
            k1data = k1
            PsiR = 10**(interp1d(np.log10(k1data),np.log10(Psi0))(np.log10(ksim))) 
            PsiR[np.isnan(PsiR)]=0    
            MuS = np.mean(PsiR)
       
        else:
            tol = 1e-4
            reltol = 5e-3
            Sarray = np.zeros(shape=(len(k1),Ny,Nz))
            Scomponent = 10*CrossSpectrumComponent1 + CrossSpectrumComponent2
    
            for ik in range(0,2):   # range(0,len(k1)):
                if PrintOutput:
                    print('ik = %s ' %(ik))
    
                klog0 = np.linspace(krange[0],krange[-1:],kdepth)
                k0p = 10**klog0        
                k0n = -np.flip(10**klog0)
                k2_p = np.concatenate((k0n,k0p), axis=0)
                k3_p = np.concatenate((k0n,k0p), axis=0)
                k2grid,k3grid=np.meshgrid(k2_p,k3_p)      
                PsiK = MannTensor(k1_in,k2_in,k3_in,Gamma,L,alphaepsilon,Scomponent)
                for iY in range(0,Ny):
                    for iZ in range(0,Nz):
                        dyi = (iY+1-1)*dy
                        dzi = (iZ+1-1)*dz
                        PsiIJ = PsiK*np.exp(  cmath.sqrt(-1)*(k2grid*dyi + k3grid*dzi))
                        ksum = TrapezoidalSum2D(PsiIJ,k2_p,k3_p)
                        Sarray[ik,iY,iZ] = ksum
    
                F1 = Sarray[ik,0,0]
                go = 1
                passcount = 0

    
    # =========================================================================
    # 3) Compute the cross-correlation structure from the spectral tensor model 
    # =========================================================================
    
    print('Calculating correlations.. ')
    
    # Allocate variables 
    ROneside = np.zeros(shape=(Nx,Ny,Nz))
    NormalizeRatio = 1
     
    # Loop 
    for iY in range(0,Ny):
        for iZ in range(0,Nz):
            dyi = (iY)*dy
            dzi = (iZ)*dz
            Sii = np.real(Sarray[:,iY,iZ])
            SiiR = alphaepsilon*(10**(interp1d(np.log10(k1data),np.log10(Sii))(np.log10(ksim))))  
            SiiR[np.isnan(SiiR)]=0
            Rii = (1/MuS)*np.real(np.fft.ifft(np.concatenate((SiiR,np.flip(np.conj(SiiR),axis=0))) ))
            if (dyi == 0) and (dzi == 0):
                NormalizeRatio = 1/Rii[0]
            
            Rii = Rii*NormalizeRatio
            RiiOneside = Rii[0:Nx]
            ROneside[:,iY,iZ] = RiiOneside
    
    
    # Grid definition     
    Yvalues = dy/2 + np.arange(0,(Ny),1)*dy
    Zvalues = dz/2 + np.arange(0,(Nz),1)*dz  
    
    # =============================================
    # Load source turbulence vector (e.g., u field) 
    # =============================================
    
    print('Loading turbulence box.. ')
    fn = r'./%s' %Input['Udatafile']
    Udata = np.fromfile(fn, np.dtype('<f'), -1)
    
    # Reshape from turbulence vector to turbulence box 
    Unorm = np.zeros(shape=(Nx,Ny,Nz))
    # Compute the mean and std for transforming into the standard normal space
    varUdata = np.var(Udata)
    muUdata = np.mean(Udata)
    stdUdata = np.sqrt(varUdata)
    
    for xloc in range(0,Nx):
        for yloc in range(0,Ny):
            dataindex = Ny*Nz*(xloc) + Ny*(yloc)
            UperZ = Udata[dataindex:dataindex+Nz]
            Unorm[xloc,yloc,:] = (UperZ - muUdata)/stdUdata
    
    # ====================
    # CONSTRAINT LOCATION 
    # ====================
    
    # Transformation in the standard normal space
    Constraints=np.asarray(Constraints)
    ConstraintValuesNorm = (Constraints[:,3] - muUdata)/stdUdata
    
    # Interpolate constrains locations to the box grid
    Clocx = interp1d(tx,np.arange(0,Nx,1),'nearest')(Constraints[:,0]).astype(int)
    Clocy = interp1d(Yvalues,np.arange(0,Ny,1),'nearest',fill_value='extrap')(Constraints[:,1]).astype(int)
    Clocz = interp1d(Zvalues,np.arange(0,Nz,1),'nearest',fill_value='extrap')(Constraints[:,2]).astype(int)
    
    # Total number constraints 
    Nconstraints = len(Constraints[:,0])
    
    # Select only unique constraints
    CpointsY = np.unique(Clocy)
    CpointsZ = np.unique(Clocz)
    
    # 
    CvectY = np.reshape(np.tile(CpointsY,(len(CpointsZ),1)).T,len(CpointsY)*len(CpointsZ),1)
    CvectZ = np.reshape(np.tile(CpointsZ,(len(CpointsY),1)),len(CpointsY)*len(CpointsZ),1)
    
    NCp = np.zeros((CvectY).shape)
    for iV in range(0,len(CvectY)):
        NCp[iV] = np.sum( np.logical_and(Clocy == CvectY[iV], Clocz == CvectZ[iV]))
    
    CPlocy = CvectY[NCp>0]
    CPlocz = CvectZ[NCp>0]
    Cplane =  np.vstack((Yvalues[CPlocy].astype(int), Yvalues[CPlocz].astype(int))).T
    # nr. of constraint points for each scans/plane    
    NCpoints = len(Cplane[:,0])
    
    
    # =========================================================================
    # 4) Compute the correlation matrix of the constraints 
    # =========================================================================    
    
    print('Assembling the covariance matrix..')
      
    # Initialize the correlation matrix of the constraints
    CorrCMann = np.zeros(shape=(Nconstraints,Nconstraints))
    CpointIndex = np.zeros(shape=(Nconstraints,1))
    
    for jCp in range(0,NCpoints):
        CpointIndex[ np.logical_and(Clocy == CPlocy[jCp], Clocz == CPlocz[jCp] )  ]= jCp
    
    CpointIndex=CpointIndex[:,0]
    for iC in range(0,Nconstraints):
        if PrintOutput:
            if np.remainder(iC,100) == 0:
                print('iC = %s' %(iC))
    
        xloci = Clocx[iC]
        yloci = Clocy[iC]
        zloci = Clocz[iC]
        
        # Computing the distance between two points: r = r1 - r2  
        xlocCij = np.abs(xloci-Clocx).astype(int)
        ylocCij = np.abs(yloci-Clocy).astype(int)
        zlocCij = np.abs(zloci-Clocz).astype(int) 
            
        Corrij = np.zeros(shape=(Nconstraints))
        for jC in range(0,Nconstraints):
            Corrij[jC] = ROneside[xlocCij[jC],ylocCij[jC],zlocCij[jC]]
        
        CorrCMann[:,iC] = Corrij
    
    # =====================
    # 5) Apply constraints
    # =====================
    
    print('Calculating constrained field..')
    
    # Allocate variables 
    UconstrainedNorm = np.zeros(shape=(Nx,Ny,Nz))
    Ucontemporaneous = np.zeros(shape=(Nconstraints,1))
    
    # Define constraints locations as int to be used as index 
    Clocx=Clocx.astype(int)
    Clocy=Clocy.astype(int)
    Clocz=Clocz.astype(int)
    
    # Unconstrained wind field at the constraints locations 
    Ucontemporaneous = np.asarray([Unorm[Clocx[iC],Clocy[iC],Clocz[iC]] for iC in range(Nconstraints)])
    
    # Compute the inverse of CorrCMann and multiply by the difference between the constrained and unconstrained field (see Eq. 2 in [1])
    CConst = linalg.lu_solve(linalg.lu_factor(CorrCMann), (ConstraintValuesNorm - Ucontemporaneous))
    
    # Pre-allocate arrays for indexing  
    xloc = np.arange(0,Nx,1) 
    dxCov1 = []
    for iC in range(0,Nconstraints):  
        dxCov = np.abs(xloc - Clocx[iC])  
        dxCov1.append(dxCov)
    dxCov1 = np.asarray(dxCov1)
        
    dyCov1 = []
    for yloc in range(0,Ny):
        dyCov = np.abs(yloc - Clocy) 
        dyCov1.append(dyCov)
    dyCov1 = np.asarray(dyCov1)
    
    dzCov1 = []
    for zloc in range(0,Nz):
        dzCov = np.abs(zloc - Clocz) 
        dzCov1.append(dzCov)
    dzCov1 = np.asarray(dzCov1)
    
    for yloc in range(0,Ny):
        for zloc in range(0,Nz):
            # Cross-correlation between the wind field and the constraints 
            Corrxyz = np.asarray([ROneside[dxCov1[iC,:],dyCov1[yloc,iC],dzCov1[zloc,iC]] for iC in range(0,Nconstraints)])    
            # Dot product (see Eq. 2 in [1])     
            CtermYZ = Corrxyz.T.dot(CConst)
            # Add constrained field to the unconstrained field realization (see Eq. 4 in [1])      
            UconstrainedNorm[:,yloc,zloc] = Unorm[:,yloc,zloc] + CtermYZ


    # Transform back to the normal space  
    Uconstrained = UconstrainedNorm*stdUdata + muUdata
    
    # =================
    # Save output file 
    # =================
    
    if SaveToFile == 1:
        # Allocate output vector 
        Uvect = np.zeros(shape=(Nx*Ny*Nz,1))
        for xloc in range(0,Nx):
            for yloc in range(0,Ny):
                dataindex = Ny*Nz*(xloc) + Ny*(yloc)
                UperZ = Uconstrained[xloc,yloc,:]
                Uvect[dataindex:dataindex+Nz,0] = UperZ                        
        
        Uvect=Uvect.astype('single')
        F = open('%s' %Input['OutputFileName'],'wb') 
        Uvect[:,0].tofile(F)
        F.close()
    
    print('DONE!!')

