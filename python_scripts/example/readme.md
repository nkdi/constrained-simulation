# Instruction to run the example 

Step1: Generate a Mann turbulence field using the mann_turb_x64.exe in (https://gitlab.windenergy.dtu.dk/nkdi/constrained-simulation/-/blob/master/mann_turb_x64.exe) and name it as "turb1001_u.bin". Use this command line: "mann_turb_x64.exe turb1001 0.07 20 3 1001 8192 32 32 0.513 3 3 true" as an example.

Step2: Generate lidar-measured wind field using ViConDAR (e.g., "uval_av_sens_dist_Sh20_SD01_V06_TI08_s1000_circ7p_vol30.mat" has been already generated)

Step3: Run the function "from_vicondar_to_cs_files.py", which creates a file (e.g., "cs_seed1000_av_circ7p_vol30.py") that containes the required inputs for running the CS framework.

Step4: Run "cs_seed1000_av_circ7p_vol30.py" file that will generate a turbulence field with lidar-based wind velocity measurements incorporated.
