# Constrained simulation scripts in python 

## Introduction 

This framework incorporates wind velocity measurements from multiple-point scanning lidars into three-dimensional wind turbulence time series serving as input to wind turbine load simulations. 

## ViConDAR 

See https://github.com/SWE-UniStuttgart/ViConDAR

## Citing

If you use the constrained simulation for a publication, use the following citation:

@article{dimitrov2017a,
  author = {Dimitrov, Nikolay Krasimirov and Natarajan, Anand},
  title = {Application of simulated lidar scanning patterns to constrained Gaussian turbulence fields for load validation},
  language = {eng},
  format = {article},
  journal = {Wind Energy},
  volume = {20},
  number = {1},
  pages = {79-95},
  year = {2017},
  issn = {10991824, 10954244},
  publisher = {John Wiley and Sons Ltd},
  doi = {10.1002/we.1992}
}

@article{wes-2020-104,
author = {Conti, D. and Pettas, V. and Dimitrov, N. and Pe\~na, A.},
title = {Wind turbine load validation in wakes using field reconstruction techniques and nacelle lidar wind retrievals},
journal = {Wind Energy Science Discussions},
volume = {2020},
year = {2020},
pages = {1--33},
url = {https://wes.copernicus.org/preprints/wes-2020-104/},
doi = {10.5194/wes-2020-104}
}
