function [Uconstrained] = ConstrainTurbulenceBox_U_S(Input)

%% ASSIGN INPUT VARIABLES
Udatafile = Input.Udatafile;
Constraints = Input.Constraints;
if ~isfield(Input,'CalculateSpectrum')
    CalculateSpectrum = 1;
else
    CalculateSpectrum = Input.CalculateSpectrum;
end
if CalculateSpectrum == 0
    CrossSpectrumFilenames = Input.CrossSpectrumFilenames;    
end
if ~isfield(Input,'CrossSpectrumComponent1')
    CrossSpectrumComponent1 = 1;
    CrossSpectrumComponent2 = 1;
else
    CrossSpectrumComponent1 = Input.CrossSpectrumComponent1;
    CrossSpectrumComponent2 = Input.CrossSpectrumComponent2;
end
Gamma = Input.Gamma;
L = Input.L;
if ~isfield(Input,'alphaepsilon')
    alphaepsilon = 1;
else
    alphaepsilon = Input.alphaepsilon;
end

if ~isfield(Input,'SpectrumSteps')
    kdepth = 120;
else
    kdepth = Input.SpectrumSteps;
end

if ~isfield(Input,'PrintDetailedOutput')
    PrintOutput = true;
else
    PrintOutput = Input.PrintDetailedOutput;
end

if ~isfield(Input,'MaxIter')
    MaxIter = 0;
end

Nx = Input.Nx;
Ny = Input.Ny;
Nz = Input.Nz;
T = Input.T;
Umean = Input.Umean;
BoxWidth = Input.BoxWidth;
BoxHeight = Input.BoxHeight;

if ~isfield(Input,'SaveToFile')
    SaveToFile = 0;
else
    SaveToFile = Input.SaveToFile;
    if ~isfield(Input,'OutputFileName')
        OutputFileName = ([Udatafile(1:end-4) '_c.bin']);
    else
        OutputFileName = Input.OutputFileName;
    end
end



%% BASIC GEOMETRY

% dx = (Umean*T)/Nx;
dy = BoxWidth/Ny;
dz = BoxHeight/Nz;

%% SPECTRAL INPUT, CROSS-CORRELATION STRUCTURE
fs = Nx/T;
dt = 1/fs;

k1range = [min([log10(1e-3/L), log10(pi/(Nx*Umean*dt))]), max([log10(1e3/L) log10(pi/(Umean*dt))])];
% k1range = [log10(pi/(Nx*Umean*dt)), log10(pi/(Umean*dt))];
% krange = [log10(1e-4/L) log10(pi/(Umean*dt))];
krange = [min([log10(1e-3/L), log10(pi/(Umean*T))]) max([log10(pi/(Umean*dt)) log10(1e3/L)])];

Nmin = 2^nextpow2(2*pi/(10.^k1range(1)*Umean*T/Nx));
Nfactor = Nmin/Nx;
Nx_s = Nfactor*Nx;
% T_s = Nfactor*T;

L0 = 1:1:Nx_s/2;
f0 = L0./(dt*Nx_s);
tx = dt*(0:(Nx-1));
% tx_s = dt*(0:(Nx_s-1));

ksim = 2*pi*f0/Umean;

k1points = 128;
% kdepth = 240;
kmin = ksim(1);
kmax = ksim(end);
kvarrange = [log10(kmin) log10(kmax)];    

k1 = 10.^linspace(kvarrange(1),kvarrange(2),k1points);

if CalculateSpectrum > 0
    disp('Calculating Mann spectrum.. ');

    if MaxIter == 0
        Sarray = zeros(length(k1),Ny,Nz);
        Scomponent = 10*CrossSpectrumComponent1 + CrossSpectrumComponent2;
        for ik = 1:length(k1)        
            if PrintOutput
                disp(['ik = ' num2str(ik)]);
            end

            klog0 = linspace(krange(1),krange(end),kdepth);
            k0p = 10.^klog0;        
            k0n = -fliplr(10.^klog0);
            k2 = [k0n k0p];
            k3 = [k0n k0p];
            [k2grid,k3grid] = meshgrid(k2,k3);        
            PsiK = MannTensor(k1(ik)*ones(size(k2grid)),ones(length(k3),1)*k2,k3'*ones(1,length(k2)),Gamma,L,alphaepsilon,Scomponent);
            for iY = 1:Ny
                for iZ = 1:Nz
                    dyi = (iY-1)*dy;
                    dzi = (iZ-1)*dz;
                    PsiIJ = PsiK.*exp( sqrt(-1)*(k2grid.*dyi + k3grid.*dzi));   
                    ksum = TrapezoidalSum2D(PsiIJ,k2,k3);
                    Sarray(ik,iY,iZ) = ksum;
                end
            end
        end

        Psi0 = Sarray(:,1,1);
        k1data = k1;        
        
    else
        tol = 1e-4;
        reltol = 5e-3;
        Sarray = zeros(length(k1),Ny,Nz);
        Scomponent = 10*CrossSpectrumComponent1 + CrossSpectrumComponent2;

        for ik = 1:length(k1)        
            if PrintOutput
                disp(['ik = ' num2str(ik)]);
            end

            klog0 = linspace(krange(1),krange(end),kdepth);
            k0p = 10.^klog0;        
            k0n = -fliplr(10.^klog0);
            k2 = [k0n k0p];
            k3 = [k0n k0p];
            [k2grid,k3grid] = meshgrid(k2,k3);        
            PsiK = MannTensor(k1(ik)*ones(size(k2grid)),ones(length(k3),1)*k2,k3'*ones(1,length(k2)),Gamma,L,alphaepsilon,Scomponent);
            for iY = 1:Ny
                for iZ = 1:Nz
                    dyi = (iY-1)*dy;
                    dzi = (iZ-1)*dz;
                    PsiIJ = PsiK.*exp( sqrt(-1)*(k2grid.*dyi + k3grid.*dzi));   
                    ksum = TrapezoidalSum2D(PsiIJ,k2,k3);
                    Sarray(ik,iY,iZ) = ksum;
                end
            end

            F1 = Sarray(ik,1,1);

            go = 1;
            passcount = 0;
    %         MaxIter = 1;

            while go == 1
                passcount = passcount + 1;
                F0 = F1;     
                klog1 = sort([klog0, klog0(1:end-1) + diff(klog0)./2]);
                k1p = 10.^klog1;
                k1n = -fliplr(10.^klog1);
                k2 = [k1n k1p];
                k3 = [k1n k1p];
                [k2grid,k3grid] = meshgrid(k2,k3);        
                PsiK = MannTensor(k1(ik)*ones(size(k2grid)),ones(length(k3),1)*k2,k3'*ones(1,length(k2)),Gamma,L,alphaepsilon,Scomponent);
                for iY = 1:Ny
                    for iZ = 1:Nz
                        dyi = (iY-1)*dy;
                        dzi = (iZ-1)*dz;
                        PsiIJ = PsiK.*exp( sqrt(-1)*(k2grid.*dyi + k3grid.*dzi));   
                        ksum = TrapezoidalSum2D(PsiIJ,k2,k3);
                        Sarray(ik,iY,iZ) = ksum;
                    end
                end

                F1 = Sarray(ik,1,1);
                res = F1 - F0;
                resrel = res/F1;
                if PrintOutput
                    disp(['Absolute residual = ' num2str(res)]);
                    disp(['Relative residual = ' num2str(resrel)]);
                end

                if abs(res) <= tol
                    if PrintOutput
                        disp(['Converged (abstol) after ' num2str(passcount) ' iterations']);
                    end
                    go = 0;
                elseif abs(resrel) <= reltol
                    if PrintOutput
                        disp(['Converged (reltol) after ' num2str(passcount) ' iterations']);
                    end
                    go = 0;
                else
                    if passcount >= MaxIter
                        if PrintOutput
                            disp(['Max number of iterations exceeded: ' num2str(passcount)]);
                        end
                        go = 0;
                    end        
                end

                klog0 = klog1;
            end
        end

        Psi0 = Sarray(:,1,1);
        k1data = k1;
    end
else
    Psi0 = MannCrossSpectrum_T(Gamma,L,alphaepsilon,krange,k1,CrossSpectrumComponent1,CrossSpectrumComponent2,0,0);
    k1data = 10.^linspace(log10(0.5e-4),log10(2e2),120);
end    
PsiR = 10.^(interp1(log10(k1),log10(Psi0),log10(ksim)));
% varU = 2*trapz(ksim,PsiR);
MuS = mean(PsiR);


% xrangeTwoside = (-Nx_s/2)*dx:dx:((Nx_s-1)/2)*dx;
% xrangeOneside = 0:dx:(Nx-1)*dx;

disp('Calculating correlations.. ');

ROneside = zeros(Nx,Ny,Nz);
% RTwoside = zeros(length(xrangeTwoside),Ny,Nz);
NormalizeRatio = 1;

for iY = 1:Ny
%     iY
    for iZ = 1:Nz
        dyi = (iY-1)*dy;
        dzi = (iZ-1)*dz;
        if CalculateSpectrum == 1
            SiRe = real(Sarray(:,iY,iZ));
        else
            if CrossSpectrumComponent1 == 1 && CrossSpectrumComponent2 == 1
                SiRe = load([CrossSpectrumFilenames num2str(Gamma) '_L' num2str(L) '_ae1_dy' num2str(dyi) '_dz' num2str(dzi) '_Re.txt']);
            else
                SiRe = load([CrossSpectrumFilenames num2str(Gamma) '_L' num2str(L) '_ae1_dy'...
                    num2str(dyi) '_dz' num2str(dzi) '_' num2str(CrossSpectrumComponent1)...
                    num2str(CrossSpectrumComponent2) '_Re.txt']);
            end
        end
%         SiIm = load([CrossSpectrumFilenames num2str(Gamma) '_L' num2str(L) '_ae1_dy' num2str(dyi) '_dz' num2str(dzi) '_Im.txt']);
%         Sii = SiRe + sqrt(-1)*SiIm;
        Sii = SiRe;
        SiiR = alphaepsilon*(10.^(interp1(log10(k1data),log10(Sii),log10(ksim))));        
%         Rii = (1/mean(SiiR))*abs(ifft([0, SiiR(2:end), 0, fliplr(conj(SiiR(2:end)))]));
%         Rii = (1/mean(SiiR))*real(ifft([SiiR, fliplr(conj(SiiR))]));
        Rii = (1/MuS)*real(ifft([SiiR, fliplr(conj(SiiR))]));
        if dyi == 0 && dzi == 0
            NormalizeRatio = 1/Rii(1);
        end
        Rii = Rii.*NormalizeRatio;
%         RiiTwoside = [Rii(Nx_s/2+1:end) Rii(1:Nx_s/2)];
        RiiOneside = Rii(1:Nx);
%         RTwoside(:,iY,iZ) = RiiTwoside;
        ROneside(:,iY,iZ) = RiiOneside;
    end
end

%% GRID DEFINITION

Yvalues = dy/2 + (0:1:(Ny-1))*dy;
Zvalues = dz/2 + (0:1:(Nz-1))*dz;



%% LOAD SOURCE TURBULENCE VECTOR
disp('Loading turbulence box.. ');
Ufile = fopen(Udatafile);
Udata = fread(Ufile,'single');
fclose(Ufile);

%% RESHAPE FROM TURBULENCE VECTOR TO TURBULENCE BOX

Unorm = zeros(Nx,Ny,Nz);
varUdata = var(Udata);
muUdata = mean(Udata);
stdUdata = sqrt(varUdata);

for xloc = 1:Nx
%     xloc
    for yloc = 1:Ny
        dataindex = Ny*Nz*(xloc - 1) + Ny*(yloc-1);
        UperZ = Udata(dataindex+1:dataindex+Nz);
        Unorm(xloc,yloc,:) = (UperZ - muUdata)./stdUdata;
    end
end

clear Udata;

ConstraintValuesNorm = (Constraints(:,4) - muUdata)./stdUdata;

%% COVARIANCE STRUCTURE

disp('Assembling the covariance matrix..');
Nconstraints = length(Constraints(:,1));

% FIND COVARIANCES WITHIN THE SOURCE TURBULENCE BOX

Clocx = interp1(tx,1:Nx,Constraints(:,1),'nearest');
Clocy = interp1(Yvalues,1:Ny,Constraints(:,2),'nearest');
Clocz = interp1(Zvalues,1:Nz,Constraints(:,3),'nearest');
CpointsY = unique(Clocy);
CpointsZ = unique(Clocz);
CvectY = reshape(CpointsY*ones(1,length(CpointsZ)),length(CpointsY)*length(CpointsZ),1);
CvectZ = reshape(ones(length(CpointsY),1)*CpointsZ',length(CpointsY)*length(CpointsZ),1);

NCp = zeros(size(CvectY));
for iV = 1:length(CvectY)
    NCp(iV) = sum(Clocy == CvectY(iV) & Clocz == CvectZ(iV));
end
CPlocy = CvectY(NCp>0);
CPlocz = CvectZ(NCp>0);
Cplane = [Yvalues(CPlocy)', Yvalues(CPlocz)'];
NCpoints = length(Cplane(:,1));


CorrCMann = zeros(Nconstraints,Nconstraints);
CpointIndex = zeros(Nconstraints,1);

for jCp = 1:NCpoints
    CpointIndex(Clocy == CPlocy(jCp) & Clocz == CPlocz(jCp)) = jCp;
end

% tic
for iC = 1:Nconstraints
    if PrintOutput
        if mod(iC,100) == 0
            disp(['iC = ' num2str(iC)]);
        end
    end
%     ti = Constraints(iC,1);
    xloci = Clocx(iC);
    yloci = Clocy(iC);
    zloci = Clocz(iC);
    xlocCij = abs(xloci-Clocx) + 1;
    ylocCij = abs(yloci-Clocy) + 1;
    zlocCij = abs(zloci-Clocz) + 1;

    Corrij = zeros(Nconstraints,1);
    for jC = 1:Nconstraints        
%         Corrij = ROneside(xlocCij,ylocCij,zlocCij);
        Corrij(jC) = ROneside(xlocCij(jC),ylocCij(jC),zlocCij(jC));
    end
    CorrCMann(:,iC) = Corrij;
    CorrCMann(iC,:) = CorrCMann(:,iC)';

end
% toc


%% APPLY CONSTRAINTS
disp('Calculating constrained field..');
UconstrainedNorm = zeros(Nx,Ny,Nz);
Ucontemporaneous = zeros(Nconstraints,1);
for iC = 1:Nconstraints
    Ucontemporaneous(iC) = Unorm(Clocx(iC),Clocy(iC),Clocz(iC));
end

CConst = CorrCMann\(ConstraintValuesNorm - Ucontemporaneous);

clear CorrCMann;


if Nconstraints <= 18000
    YZcount = 0;

    for yloc = 1:Ny
        for zloc = 1:Nz
            xloc = (1:Nx)';
            dyCov = abs(yloc - Clocy) + 1;
            dzCov = abs(zloc - Clocz) + 1;
            YZcount = YZcount + 1;
            if PrintOutput
                disp(['YZcount = ' num2str(YZcount)]);    
            end
        Corrxyz = zeros(Nx,Nconstraints);
        for iC = 1:Nconstraints
            dxCov = abs(xloc - Clocx(iC)) + 1;
    %         Ri = Rvect(DXvect == dxCov(iC) & DYvect == dyCov(iC) & DZvect == dzCov(iC));
            Corrxyz(:,iC) = ROneside(dxCov,dyCov(iC),dzCov(iC));
        end

        CtermYZ = Corrxyz*CConst;
        UconstrainedNorm(:,yloc,zloc) = Unorm(:,yloc,zloc) + CtermYZ;

        end 
    end  
%     YZcount = 0;
% 
%     for yloc = 1:Ny
%         for zloc = 1:Nz
% %             yloc = 5;
% %             zloc = 17;
% %     for yloc = 16:17
% %         for zloc = 16:17
%             YZcount = YZcount + 1;
%             disp(['YZcount = ' num2str(YZcount)]);
%             Corrxyz = zeros(Nx,Nconstraints);
%             for Cloc = 1:NCpoints
%                 dyloc = abs(CPlocy(Cloc) - yloc) + 1;
%                 dzloc = abs(CPlocz(Cloc) - zloc) + 1;               
%                 CorrMannYZ = ROneside(:,dyloc,dzloc);
%                 for xloc = 1:Nx
%                     dxCov = abs(xloc-Clocx) + 1;
%                     Corrxyz(xloc,CpointIndex==Cloc) = CorrMannYZ(dxCov(CpointIndex==Cloc));
%                 end
%             end
% 
%             CtermYZ = Corrxyz*CConst;
%             UconstrainedNorm(:,yloc,zloc) = Unorm(:,yloc,zloc) + CtermYZ;
%         end
%     end
    
else


    for xloc = 1:Nx
    %     if mod(xloc,100) == 0
            disp(['xloc = ' num2str(xloc)]);
    %     end
        dxCov = abs(xloc-Clocx) + 1;

        for yloc = 1:Ny
            for zloc = 1:Nz
                Corrxyz = zeros(1,Nconstraints);
                    for Cloc = 1:NCpoints
                        dyloc = abs(CPlocy(Cloc) - yloc) + 1;
                        dzloc = abs(CPlocz(Cloc) - zloc) + 1;                                
    %                     CorrMannYZ = ROneside(:,dyloc,dzloc);             
    %                     Corrxyz(CpointIndex==Cloc) = CorrMannYZ(dxCov(CpointIndex==Cloc));
                        Corrxyz(CpointIndex==Cloc) = ROneside(dxCov(CpointIndex==Cloc),dyloc,dzloc);
                    end
                UconstrainedNorm(xloc,yloc,zloc) = Unorm(xloc,yloc,zloc) + Corrxyz*CConst;
            end
        end
    end

end

clear Unorm;
clear Corrxyz;

Uconstrained = UconstrainedNorm.*stdUdata + muUdata;

%% SAVE OUTPUT FILE

if SaveToFile == 1
    Uvect = zeros(Nx*Ny*Nz,1);
for xloc = 1:Nx
%     xloc
    for yloc = 1:Ny
        dataindex = Ny*Nz*(xloc - 1) + Ny*(yloc-1);
        UperZ = Uconstrained(xloc,yloc,:);
        Uvect(dataindex+1:dataindex+Nz) = UperZ;                        
    end
end    
    OutputFile = fopen(OutputFileName,'w');
    fwrite(OutputFile,Uvect,'single');    
    fclose(OutputFile);
end

disp('DONE!!');