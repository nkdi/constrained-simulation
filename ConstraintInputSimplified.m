close all
clear
clc

%% BASIC INPUTS

MannL = 72;
MannGamma = 3.9;
MannAE = 1;
Umean = 6;

%% FILE INPUT
UnconstrainedTurbulenceBoxFilename = 'turb1001_u.bin';
TurbulenceConstrainedFilename = 'turb1001_u_c.bin';

%% BASIC GEOMETRY AND GRID DEFINITION
Nx = 8192;
Ny = 32;
Nz = 32;

T = (Nx/8192)*700; % 700-sec duration, first 100 seconds for initialization

BoxWidth = 179.2; % DTU 10MW box size
BoxHeight = 179.2; % DTU 10MW box size

Yhub = 89.6; % Distance from lower left corner of t-box to hub
Zhub = 89.6;

dx = (Umean*T)/Nx;
dy = BoxWidth/Ny;
dz = BoxHeight/Nz;

Yvalues = dy/2 + (0:1:(Ny-1))*dy;
Zvalues = dz/2 + (0:1:(Nz-1))*dz;

Yplane = Yvalues'*ones(1,length(Zvalues));
Zplane = ones(length(Yvalues),1)*Zvalues;

TurbFileStrings = strsplit(UnconstrainedTurbulenceBoxFilename,'_');
TurbulencePrefix = TurbFileStrings{1};
MannGenCommand = ['c:\Hawc2\Mann64Bit\mann_turb_x64.exe ' TurbulencePrefix ...
    ' ', num2str(MannAE), ' ', num2str(MannL), ' ', num2str(MannGamma), ' ', num2str(T),...
    ' ', num2str(Nx),' ', num2str(Ny),' ', num2str(Nz), ' ' , num2str(dx), ' ',...
    num2str(dy), ' ', num2str(dz), ' 1'];

dos(MannGenCommand);


%% CONSTRAINT DEFINITION

Constraints = [1 86.8 86.8 0;
               60 86.8 86.8 5;
               300 86.8 86.8 10;
               400 86.8 86.8 10;
               500 86.8 86.8 -10;
               600 86.8 86.8 15]; % [t, y, z, u-value]


% INPUT TO CONSTRAINING PROCEDURE

Input.Udatafile = UnconstrainedTurbulenceBoxFilename;
Input.CalculateSpectrum = 1;
Input.SpectrumSteps = 60;
Input.Gamma = MannGamma;
Input.L = MannL;
Input.alphaepsilon = 1;
Input.Nx = Nx;
Input.Ny = Ny;
Input.Nz = Nz;
Input.T = T;
Input.Umean = Umean;
Input.BoxWidth = BoxWidth;
Input.BoxHeight = BoxHeight;
Input.SaveToFile = 1;
Input.PrintDetailedOutput = 0;

Input.OutputFileName = TurbulenceConstrainedFilename;
Input.CrossSpectrumComponent1 = 1;
Input.CrossSpectrumComponent2 = 1;        
Input.Constraints = Constraints; % 6-column vector of constraints: [t,y,z,uvalue,vvalue,wvalue]
disp('Start applying constraints:');
tic
UconstrainedRes = ConstrainTurbulenceBox_U_S(Input);
toc

    
% TEST RESULTS
% Uref = zeros(Nx,Ny,Nz);
Uunc = zeros(Nx,Ny,Nz);
Ucon = zeros(Nx,Ny,Nz);
   
Uuncfile = fopen(UnconstrainedTurbulenceBoxFilename);
Uuncdata = fread(Uuncfile,'single');
fclose(Uuncfile);   

Uconfile = fopen(TurbulenceConstrainedFilename);
Ucondata = fread(Uconfile,'single');
fclose(Uconfile);   

   for ClocX = 1:Nx
        for yloc = 1:Ny
            dataindex = Ny*Nz*(ClocX - 1) + Ny*(yloc-1);
            UperZunc = Uuncdata(dataindex+1:dataindex+Nz);
            UperZcon = Ucondata(dataindex+1:dataindex+Nz);
            Uunc(ClocX,yloc,:) = UperZunc;
            Ucon(ClocX,yloc,:) = UperZcon;
        end             
   end     

   tx = (0:1:Nx-1).*T/Nx;
   % Choose only constraints close to the hub location for plotting
    Cindex = (Constraints(:,2) == 86.8) & (Constraints(:,3) == 86.8); 

    figure()
    plot(tx,Uunc(:,16,16),'--b');
    hold on
    plot(tx,Ucon(:,16,16),'-k');    
    plot(Constraints(Cindex,1),Constraints(Cindex,4),'or','Linewidth',1,'Markersize',5);
    legend('Source time series','Constrained time series','Constraints');
             
